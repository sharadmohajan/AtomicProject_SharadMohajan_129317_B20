<html>
    <head>
        <style>
            #header {
                background-color:black;
                color:white;
                text-align:center;
                padding:5px;
            }
            #nav {
                line-height:30px;
                background-color:#eeeeee;
                height:370px;
                width:100px;
                float:center;
                padding:5px;
            }
            #section {
                width:350px;
                float:left;
                padding:10px;
            }
            #footer {
                background-color:black;
                color:white;
                clear:both;
                text-align:center;
                padding:5px;
            }
        </style>
    </head>
<body>

<div id="header">
    <h1>Welcome To My BITM Project</h1>
</div>

<div id="nav">
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/BookTitle/">
        <button type="submit">Book Title</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/BirthDate/">
        <button type="submit">Birth Date</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/OrganizationSummary/">
        <button type="submit">Summary</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/Subscription/">
        <button type="submit">Subscription</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/ProfilePicture/">
        <button type="submit">Picture</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/Gender/">
        <button type="submit">Gender</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/TermsAndConditions/">
        <button type="submit">Terms</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/Hobby/">
        <button type="submit">Hobby</button>
    </form>
    <form method="get" action="/AtomicProject_SharadMohajan_129317_B20/views/SEIP129317/City/">
        <button type="submit">City</button>
    </form>
</div>

<div id="section">


</div>

<div id="footer">
    Copyright � Sharad Mohajan
</div>

</body>
</html>